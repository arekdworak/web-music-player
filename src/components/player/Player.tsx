import React, { Fragment } from 'react';
import { Song } from '../../state';
import { VisibleComponent, VisibleComponentProps } from './../visible-component';
import './player.css';

interface PlayerProps extends VisibleComponentProps {
    currentSong?: Song;
    isPaused: boolean;
    onHamburgerButtonClick: () => void;
    onPlayPause: () => void;
    onNextButtonClick: () => void;
    onPrevButtonClick: () => void;
}

export default class Player extends VisibleComponent<PlayerProps> {

    renderContent(): any {
        return (
            <Fragment>
                <div className="image">
                    <div className="top-buttons">
                        <div className="buttons">
                            <div className="left-filler"></div>
                            <div className="options">
                                <i className="fas fa-recycle"></i>
                                <i className="fas fa-random"></i>
                                <i className="fas fa-redo"></i>
                            </div>
                            <div className="hamburger" onClick={this.props.onHamburgerButtonClick}>
                                <i className="fas fa-bars"></i>
                            </div>
                        </div>
                        <div className="shadow"></div>
                    </div>
                    <div className="song-info">
                        <div className="info">
                            <div className="author">{this.props.currentSong && this.props.currentSong.author}</div>
                            <div className="title">{this.props.currentSong && this.props.currentSong.title}</div>
                        </div>
                        <div className="shadow"></div>
                    </div>
                </div>
                <div className="menu">
                    <div className="share">
                        <i className="fas fa-share-alt"></i>
                    </div>
                    <div className="play-nav">
                        <div className="prev" onClick={this.props.onPrevButtonClick}>
                            <i className="fas fa-step-backward"></i>
                        </div>
                        <div className="play-pause" onClick={this.props.onPlayPause}>
                            <i className={`fas ${this.props.isPaused ? 'fa-play' : 'fa-pause'}`}></i>
                        </div>
                        <div className="next" onClick={this.props.onNextButtonClick}>
                            <i className="fas fa-step-forward"></i>
                        </div>
                    </div>
                    <div className="heart">
                        <i className="fas fa-heart"></i>
                    </div>
                </div>
            </Fragment>
        );
    }
}