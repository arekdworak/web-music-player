import React, { Component} from 'react';

export interface VisibleComponentProps {
    cssClass: string;
    isVisible: boolean;
}


export abstract class VisibleComponent<P extends VisibleComponentProps> extends Component<P> {

    render(): any {
        return <div className={`${this.props.cssClass} ${!this.props.isVisible ? 'hidden': ''} visible-component`}>
            {this.renderContent()}
        </div>
    }

    abstract renderContent(): any;

}