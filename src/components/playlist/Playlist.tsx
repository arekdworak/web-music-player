import React, { Fragment } from 'react';
import { Song } from './../../state';
import { VisibleComponent, VisibleComponentProps } from './../visible-component';
import './playlist.css';

interface Style {
    height: string;
}

interface PlaylistState {
    style: Style;
}

interface PlaylistProps extends VisibleComponentProps {
    songs: Array<Song>;
    onReturnButtonClick: () => void;
    onSongClick: (song: Song, hidePlaylist: boolean) => void;
}

export default class Playlist extends VisibleComponent<PlaylistProps> {

    state: PlaylistState = {
        style : {
            height: 'auto'
        }
    }

    componentDidMount() {
        this.onResize();
        window.addEventListener('resize', this.onResize);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.onResize);
    }


    renderContent(): any {
        return (
            <Fragment>
                <h3>
                    <i className="fas fa-reply" onClick={this.props.onReturnButtonClick}></i>
                    <span className="playlist-title">Playlist</span>
                    <div className="right-filler"></div>
                </h3>
                <div className="songs" style={this.state.style}>
                    {this.renderSongs()}
                </div>
            </Fragment>
        );
    }

    private renderSongs(): any {
        return this.props.songs.map((song: Song) => {
            return <div className="song-details" key={song.id}>
                <div className="info" onClick={() => { this.props.onSongClick(song, true) }}>
                    <div className="top">
                        <div className="time">{song.time}</div>
                        <div className="author">{song.author}</div>
                    </div>
                    <div className="bottom">
                        <div className="title">{song.title}</div>
                    </div>
                </div>
                <div className="icons">
                    <i className="fas fa-share-alt"></i>
                    <i className="fas fa-heart"></i>
                </div>
            </div>
        });
    }

    private onResize = (): void => {
        this.setState({
            style: {
                height: `${window.innerHeight - 220}px`
            }
        });
    }
}