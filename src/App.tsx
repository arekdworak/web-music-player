import React, { Component } from 'react';
import './App.css';
import Player from './components/player';
import Playlist from './components/playlist';
import songs from './songs.json';
import { AppState, Song } from './state';

export default class App extends Component {

  state: AppState = {
    songs,
    isPaused: true,
    playlistVisible: false
  };


  render() {
    return (
      <div className="app">
        <Player
          isVisible={!this.state.playlistVisible}
          cssClass="player"
          currentSong={this.state.currentSong}
          isPaused={this.state.isPaused}
          onHamburgerButtonClick={this.showPlaylist}
          onPlayPause={this.playPause}
          onNextButtonClick={this.setNextSong}
          onPrevButtonClick={this.setPrevSong}>
        </Player>
        <Playlist
          cssClass="playlist"
          isVisible={this.state.playlistVisible}
          songs={this.state.songs}
          onReturnButtonClick={this.hidePlaylist}
          onSongClick={this.setCurrentSong}>
        </Playlist>
      </div>
    );
  }

  componentDidMount() {
    this.setCurrentSong(this.state.songs[0]);
    this.playPause();
  }

  private showPlaylist = (): void => {
    this.setState({
      playlistVisible: true
    });
  }

  private hidePlaylist = (): void => {
    this.setState({
      playlistVisible: false
    });
  }

  private playPause = (): void => {
    this.setState({
      isPaused: !this.state.isPaused
    });
  }

  private setCurrentSong = (song: Song, hidePlaylist?: boolean): void => {
    if (hidePlaylist) {
      this.hidePlaylist();
    }
    if (this.state.isPaused) {
      this.setState({ currentSong: song, isPaused: false });
    } else {
      this.setState({ currentSong: song });
    }
  }

  private setNextSong = (): void => {
    if (this.state.currentSong) {
      const currentSongIndex: number = this.state.songs.indexOf(this.state.currentSong);
      const lastSongIndex: number = this.state.songs.length - 1;
      if (currentSongIndex === lastSongIndex) {
        this.setCurrentSong(this.state.songs[0]);
      } else {
        this.setCurrentSong(this.state.songs[currentSongIndex + 1]);
      }
    }
  }

  private setPrevSong = (): void => {
    if (this.state.currentSong) {
      const currentSongIndex: number = this.state.songs.indexOf(this.state.currentSong);
      const lastSongIndex: number = this.state.songs.length - 1;
      if (currentSongIndex === 0) {
        this.setCurrentSong(this.state.songs[lastSongIndex]);
      } else {
        this.setCurrentSong(this.state.songs[currentSongIndex - 1]);
      }
    }
  }
}
