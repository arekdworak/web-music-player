export interface Song {
    id: number;
    title: string;
    author: string;
    time: string;
}

export interface AppState {
    songs: Array<Song>;
    currentSong?: Song;
    isPaused: boolean;
    playlistVisible: boolean;
}