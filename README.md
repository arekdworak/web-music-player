# Web-Based Music Player
## Used technology
1. React with ES6 and TypeScript
2. Font Awesome
## How to run
1. Download or clone repo
2. Go to project directory
3. `npm i`
4. `npm start`
